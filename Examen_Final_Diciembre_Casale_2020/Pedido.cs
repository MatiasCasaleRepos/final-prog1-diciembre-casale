﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_Final_Diciembre_Casale_2020
{
    public class Pedido
    {
        public int NroPedido { get; set; }
        public int DNICliente { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaEntregaEstimada { get; set; }
        public double CostoTotal { get; set; }
        public FormaPago FormaPagoSeleccionada { get; set; }
        public DateTime FechaPago { get; set; }
    }

    public enum FormaPago
    {
        Efectivo, Transferencia, Mercadopago
    }
}
