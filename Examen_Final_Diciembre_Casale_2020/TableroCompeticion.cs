﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_Final_Diciembre_Casale_2020
{
    public class TableroCompeticion : Juego
    {
        public string Color { get; set; }
        public Categoria CategoriaTablero { get; set; }
        public int CantJugadoresPosibles { get; set; }

        public override string ObtenerDescripcion()
        {
            return $"Tipo: Competicion | {base.ObtenerDescripcion()} | Cantidad de jugadores: {CantJugadoresPosibles} | ¿Es personalizado?: {CategoriaTablero.ToString()}";
        }
    }

    public enum Categoria
    {
        Deporte, Didáctico, Estrategia
    }
}