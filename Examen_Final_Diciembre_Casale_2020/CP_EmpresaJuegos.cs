﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_Final_Diciembre_Casale_2020
{
    public class CP_EmpresaJuegos
    {
        List<Pedido> ListaPedidos = new List<Pedido>();
        List<Cliente> ListaCliente = new List<Cliente>();
        List<Juego> ListaJuegos = new List<Juego>();
        
        public void ObtenerDescripcionProducto()
        {
            ObtenerDescripcion();
        }

        public Resultado DarAltaCliente(Cliente nuevoCliente)
        {
            Resultado resultado = new Resultado();
            if (ListaCliente.Exists(x => x.DNI == nuevoCliente.DNI))
            {
                resultado.ResultadoOperacion = false;
                resultado.Mensaje = "Error: El cliente ya existe!";                
            }
            else
            {
                //Corrección: Si  la propiedad teléfono es int, su valor por defecto es 0 y por ello hay que controlar. Si es int? estaría bien con el null
                if (nuevoCliente.Nombre == null || nuevoCliente.Domicilio == null || nuevoCliente.Telefono == null)
                {
                    resultado.ResultadoOperacion = false;
                    resultado.Mensaje = "Error: Faltan Datos Importantes!";
                }
                else
                {
                    ListaCliente.Add(nuevoCliente);
                    resultado.ResultadoOperacion = true;
                    resultado.Mensaje = "Se registro el nuevo cliente!";
                }
            }
            return resultado;
        }

        public void RegistrarNuevoPedido(int dniCliente, int nroTablero)
        {
            Cliente encontrarCliente = ListaCliente.Find(x => x.DNI == dniCliente);
            Juego encontrarTabero = ListaJuegos.Find(x => x.Cod == nroTablero);
            int contDias = 0;             

            Pedido nuevoPedido = new Pedido() { DNICliente = dniCliente, FechaCreacion = DateTime.Today, NroPedido = ListaPedidos.Count + 1 };

            //Corrección: PAra esto se necesitaba que sea un método abstracto a nivel de juego y que sea implementado en cada subclase.
            if (encontrarTabero is TableroSensorial)
            {
                TableroSensorial juego = encontrarTabero as TableroSensorial;

                if (juego.PoseeDiseñoPersonalizado)
                    contDias = 14;
                else
                    contDias = 10;
            }
            else
            {
                if (encontrarTabero is TablerosProno)
                {
                    TablerosProno juego = encontrarTabero as TablerosProno;

                    if (juego.CantBarras <= 3)
                        contDias = 10;
                    else
                        contDias = 15;

                    if (juego.ListaColores.Count == 3)
                        contDias += 2;
                }
                else
                {
                    TableroCompeticion juego = encontrarTabero as TableroCompeticion;

                    if (juego.CategoriaTablero == Categoria.Estrategia)
                        contDias = 10;
                    else
                        contDias = 15;
                }
            }
            nuevoPedido.FechaEntregaEstimada.AddDays(+contDias);
            nuevoPedido.CostoTotal = encontrarTabero.PrecioBase + (contDias * 80);
            ListaPedidos.Add(nuevoPedido);
        }

        public void ActualizarEstadoPedido(int idPedido, FormaPago formapago)
        {
            ListaPedidos[ListaPedidos.FindIndex(x => x.NroPedido == idPedido)].FormaPagoSeleccionada = formapago; 
            ListaPedidos[ListaPedidos.FindIndex(x => x.NroPedido == idPedido)].FechaPago = DateTime.Today;
        }
    }
}
