﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_Final_Diciembre_Casale_2020
{
    public class Juego
    {
        public int Cod { get; set; }
        public int Largo { get; set; }
        public int Ancho { get; set; }
        public int EdadMinimaRecomendada { get; set; }
        public int EdadMaximaRecomendada { get; set; }
        public double PrecioBase { get; set; }

        //Corrección: Falta un método para calcular la cantidad de día de entrega.

        public virtual string ObtenerDescripcion()
        {
            return $"Código: {Cod} | Tamaño: {Largo}x{Ancho}";
        }
    }
}
