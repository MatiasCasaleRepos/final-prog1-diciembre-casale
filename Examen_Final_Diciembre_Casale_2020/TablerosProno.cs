﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_Final_Diciembre_Casale_2020
{
    public class TablerosProno : Juego
    {
        public int CantBarras { get; set; }
        public bool PoseeDiseñoPersonalizado { get; set; }
        public List<string> ListaColores { get; set; }

        public override string ObtenerDescripcion()
        {
            string mensaje;
            if (PoseeDiseñoPersonalizado)
                mensaje = "Si";
            else
                mensaje = "No";
            return $"Tipo: Prono | {base.ObtenerDescripcion()} | Cantidad de barras:  {CantBarras} | ¿Es personalizado?: {mensaje}";
        }
    }
}
