﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_Final_Diciembre_Casale_2020
{
    public class TableroSensorial : Juego
    {
        public int CantJuegosIncluidos { get; set; }
        public string Color { get; set; }
        public bool PoseeDiseñoPersonalizado { get; set; }

        public override string ObtenerDescripcion()
        {
            string mensaje;
            if (PoseeDiseñoPersonalizado)
                mensaje = "Si";
            else
                mensaje = "No";
            return $"Tipo: Sensorial | {base.ObtenerDescripcion()} | Cantidad de juegos: {CantJuegosIncluidos} | ¿Es personalizado?: {mensaje}" ;
        }
    }
}
