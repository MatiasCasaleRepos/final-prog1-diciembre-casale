﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_Final_Diciembre_Casale_2020
{
    public class Cliente
    {
        public int DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int CP { get; set; }
        public string Domicilio { get; set; }
        public string Email { get; set; }
        public int Telefono { get; set; }
    }
}
